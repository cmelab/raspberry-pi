#Import necessary packages

import spidev
from time import sleep, strftime, time
import matplotlib.pyplot as plt
import numpy as np

#Define variables

delay = 0.25
ldr_channel = 0

#Create SPI

spi = spidev.SpiDev()
spi.open(0, 0)

#Read SPI data from MCP3008, 8 channels total

def readadc(adcnum):
	if adcnum > 7 or adcnum < 0:
		return -1
	r = spi.xfer2([1,8 + adcnum << 4,0])
	data = ((r[1] & 3) << 8) + r[2]
	return data

#Write data to csv file


light = readadc(ldr_channel)

def write_light(light):
	with open("digilight.csv","a") as log:
		log.write("{0},{1}\n".format(strftime("%Y-%m-%d %H%M%S"),str(light)))

#Generate plot

#plt.ion()
#x = []
#y = []

#def graph(digilight):
	#y.append(digilight)
	#x.append(time())
	#plt.plot(x,y)


#Print values
try:

	while True:
		ldr_value = readadc(ldr_channel)
		write_light (ldr_value)
		print (ldr_value)
		sleep(delay)
		
except KeyboardInterrupt:
	pass


	
