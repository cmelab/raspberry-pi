#Run a photoresistor and a temperature sensor on Raspberry Pi
import spidev
from time import sleep, strftime, time
import os

#Open SPI bus
spi = spidev.SpiDev()
spi.open(0,0)

#Define variables
delay = .5
ldr_channel = 0
tmp_channel = 1

#Read data from ADC
def ReadChannel(channel):
	adc = spi.xfer2([1,(8+channel)<<4,0])
	data = ((adc[1]&3) << 8) + adc[2]
	return data

#Convert data to voltage
def ConvertVolts(data,places):
	volts = (data * 3.3) / float(1023)
	volts = round(volts,places)
	return volts

#Calculate temp data
def ConvertTemp(data,places):
	temp = (data * 330/float(1023))-50
	temp = round(temp,places)
	return temp

#Writing data to .csv

light = ReadChannel(ldr_channel)
temper = ReadChannel(tmp_channel)

def write_light(light):
	with open("combolight.csv","a") as log:
		log.write("{0},{1}\n".format(strftime("%Y-%m-%d %H%M%S"),str(light))) 
def write_temp(temper):
	with open("combotemp.csv","a") as log:
		log.write("{0},{1}\n".format(strftime("%Y-%m-%d %H%M%S"),str(temper)))

while True:

#Read LDR data
	light_level = ReadChannel(ldr_channel)
	light_volts = ConvertVolts(light_level,2)

#Read Temp data
	temp_level = ReadChannel(tmp_channel)
	temp_volts = ConvertVolts(temp_level,2)
	temp = ConvertTemp(temp_level,2)

#Print results
	print "-------------------------------------------"
	print ("Light: {} ({})V".format(light_level,light_volts))
	print ("Temp: {} ({})V {} deg C".format(temp_level,temp_volts,temp))
	write_light(light_volts)
	write_temp(temp_volts)

#Delay before gathering more data
	sleep(delay)


