#This code is to be used with a piezo speaker and a photoresistor.  The frequency of the speaker is dependent on the reisitance of the photoresistor.


#Import necessary packages

import spidev
from time import sleep, strftime, time
import RPi.GPIO as GPIO


#Define variables

delay = 0.25
ldr_channel = 0

#Create SPI

spi = spidev.SpiDev()
spi.open(0, 0)

#Set GPIO pins

piezo = 7
GPIO.setmode(GPIO.BOARD)
GPIO.setup(piezo, GPIO.OUT)

#Read SPI data from MCP3008, 8 channels total

def readadc(adcnum):
	if adcnum > 7 or adcnum < 0:
		return -1
	r = spi.xfer2([1,8 + adcnum << 4,0])
	data = ((r[1] & 3) << 8) + r[2]
	return data

#Print values
try:

	while True:
		ldr_value = readadc(ldr_channel)
		freq_s = 150000/ldr_value
		freq = float(freq_s)
		duration_s = .25
		duration = float(duration_s)
		period = 1.0/freq
		delay = period/2
		cycles = int(duration/period)
		print (ldr_value)
		sleep(delay)
			
		for i in range(cycles):
			GPIO.output(piezo, True)
			sleep(delay)
			GPIO.output(piezo, False)
			sleep(delay)

except KeyboardInterrupt:
	pass

finally:
	GPIO.cleanup()
	
