#This code prints values from a Force Sensitive Resistor and emits tone using a pizeo buzzer.  
#The tone emitted by the pizeo buzzer is dependent on the resistance measured from the fsr.


#Import packages

import spidev
import time
import RPi.GPIO as GPIO

#Define variables

fsr_channel = 2
		

#Create SPI

spi = spidev.SpiDev()
spi.open(0, 0)

#Read data on channel 2 from MCP3008

def readadc(adcnum):
	if adcnum > 7 or adcnum < 0:
		return -1
	r = spi.xfer2([1,8 + adcnum << 4,0])
	data = ((r[1] & 3) << 8) + r[2]
	return data






#Set I/O pins

piezo = 7
GPIO.setmode(GPIO.BOARD)
GPIO.setup(piezo, GPIO.OUT)




#Run program & print values

try:
	while True:
		fsr_value = readadc(fsr_channel) + 1
		freq_s = fsr_value * 10
		freq = float(freq_s)
		duration_s = .25
		duration = float(duration_s)
		period = 1/freq
		delay = period/2
		cycles = int(duration/period)
		print (fsr_value)
		time.sleep(delay)
		print (freq)
		time.sleep(delay)
			

		for i in range(cycles):
			GPIO.output(piezo, False)
			time.sleep(delay)
			GPIO.output(piezo, True)
			time.sleep(delay)


except KeyboardInterrupt:
	pass

finally:
	GPIO.cleanup()
