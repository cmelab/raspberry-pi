#This code prints values from a Force Sensitive Resistor hooked up to a Raspberry Pi

#Import packages

import spidev
from time import sleep, strftime, time

#Define variables

delay = 0.25
fsr_channel = 2

#Create SPI

spi = spidev.SpiDev()
spi.open(0, 0)

#Read data on channel 2 from MCP3008

def readadc(adcnum):
	if adcnum > 7 or adcnum < 0:
		return -1
	r = spi.xfer2([1,8 + adcnum << 4,0])
	data = ((r[1] & 3) << 8) + r[2]
	return data

#Setup file to write data to

force = readadc(fsr_channel)

def write_force(force):
	with open("force.csv","a") as log:
		log.write("{0},{1}\n".format(strftime("%Y-%m-%d %H%M%S"),str(force)))

#Run program & print values

try:
	while True:
		fsr_value = readadc(fsr_channel)
		write_force (fsr_value)
		print (fsr_value)
		sleep(delay)

except KeyboardInterrupt:
	pass


