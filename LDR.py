import RPi.GPIO as GPIO
from time import sleep, strftime, time
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import sys

GPIO.setmode(GPIO.BOARD)

pin_to_circuit = 7

plt.ion=()
x = []
y = []

def rc_time (pin_to_circuit) :
	count = 0
	
	GPIO.setup(pin_to_circuit, GPIO.OUT)
	GPIO.output(pin_to_circuit, GPIO.LOW)
	sleep(0.1)

	GPIO.setup(pin_to_circuit, GPIO.IN)

	while (GPIO.input(pin_to_circuit) == GPIO.LOW) :
		count += 1

	return count
		
light = rc_time(pin_to_circuit)

def write_light(light):
	with open("light.csv","a") as log:
		log.write("{0},{1}\n".format(strftime("%Y-%m-%d %H:%M:%S"),str(light)))

def graph(light):
	y.append(light)
	x.append(time())
	plt.scatter(x,y)
	plt.plot(x,y)
	plt.show()

try:

	while True:
		print rc_time(pin_to_circuit)
		write_light(light)
		graph(light)

except KeyboardInterrupt:
	pass

finally:
	GPIO.cleanup()
