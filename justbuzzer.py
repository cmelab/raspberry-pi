import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)

buzzer_pin = 7

GPIO.setmode(GPIO.BOARD)
GPIO.setup(buzzer_pin,GPIO.OUT)

freq_s = input('Enter Pitch (200 to 2000):')
freq = float(freq_s)
duration_s = input('Enter Duration (seconds):')
duration = float(duration_s)
period = 1.0/freq
delay = period/2
cycles = int(duration/period)

for i in range(cycles):
	GPIO.output(buzzer_pin, True)
	time.sleep(delay)
	GPIO.output(buzzer_pin, False)
	time.sleep(delay)
